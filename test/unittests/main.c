// Copyright 2016 ELIFE. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include <unit.h>

IMPORT_SUITE(Canary);
IMPORT_SUITE(ElementaryCAs);
IMPORT_SUITE(OpenElementaryCAs);

BEGIN_REGISTRATION
  REGISTER(Canary)
  REGISTER(ElementaryCAs)
  REGISTER(OpenElementaryCAs)
END_REGISTRATION

UNIT_MAIN();
