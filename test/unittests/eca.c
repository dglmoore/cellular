// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include <unit.h>
#include <cellular.h>

UNIT(ECANew)
{
  cellular_eca rule = cellular_eca_new(30);
  ASSERT_EQUAL(rule.code, 30);
}

UNIT(ECARule30_Update)
{
  int expect[5] = {0,1,1,1,0};
  int got[5] = {0,0,1,0,0};
  cellular_eca rule = cellular_eca_new(30);
  cellular_automaton_update(got, sizeof(got)/sizeof(int), &rule);
  for (size_t i = 0; i < 5; ++i)
  {
    ASSERT_EQUAL(expect[i], got[i]);
  }
}

UNIT(ECARule30Bounds_Update)
{
  int expect[5] = {1,1,0,0,1};
  int got[5] = {1,0,0,0,0};
  cellular_eca rule = cellular_eca_new(30);
  cellular_automaton_update(got, sizeof(got)/sizeof(int), &rule);
  for (size_t i = 0; i < 5; ++i)
  {
    ASSERT_EQUAL(expect[i], got[i]);
  }
}

UNIT(ECARule30LongTime_Update)
{
  int expect[41] = {0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,
                    1,0,0,1,1,0,0,0,1,1,1,1,1,1,0,0,1,0,1,0};
  int got[41];
  for (size_t i = 0; i < 41; ++i)
  {
    got[i] = (i == 20);
  }

  cellular_eca rule = cellular_eca_new(30);
  for (size_t i = 0; i < 10000; ++i)
  {
    cellular_automaton_update(got, sizeof(got)/sizeof(int), &rule);
  }

  for (size_t i = 0; i < 41; ++i)
  {
    ASSERT_EQUAL(expect[i], got[i]);
  }
}

UNIT(ECARule30_Step)
{
  int expect[5] = {0,1,1,1,0};
  int init[5] = {0,0,1,0,0};
  int got[5];
  cellular_eca rule = cellular_eca_new(30);
  int n = cellular_automaton_step(got, init, sizeof(init)/sizeof(int), &rule);
  ASSERT_NOT_EQUAL(n, 0);
  for (size_t i = 0; i < 5; ++i)
  {
    ASSERT_EQUAL(expect[i], got[i]);
  }
}

UNIT(ECARule30Bounds_Step)
{
  int expect[5] = {1,1,0,0,1};
  int init[5] = {1,0,0,0,0};
  int got[5];
  cellular_eca rule = cellular_eca_new(30);
  int n = cellular_automaton_step(got, init, sizeof(init)/sizeof(int), &rule);
  ASSERT_NOT_EQUAL(n, 0);
  for (size_t i = 0; i < 5; ++i)
  {
    ASSERT_EQUAL(expect[i], got[i]);
  }
}

UNIT(ECARule30LongTime_Step)
{
  size_t const N = 41;

  int expect[41] = {0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,
                   1,0,0,1,1,0,0,0,1,1,1,1,1,1,0,0,1,0,1,0};

  int init[41], got[41];
  for (size_t i = 0; i < N; ++i) init[i] = (i == N/2);

  cellular_eca rule = cellular_eca_new(30);
  for (size_t i = 0; i < 10000; ++i)
  {
    int n = cellular_automaton_step(got, init, N, &rule);
    ASSERT_EQUAL(n, N);
    memcpy(init, got, N*sizeof(int));
  }

  for (size_t i = 0; i < N; ++i)
  {
    ASSERT_EQUAL(expect[i], init[i]);
  }
}

BEGIN_SUITE(ElementaryCAs)
  ADD_UNIT(ECANew)

  ADD_UNIT(ECARule30_Update)
  ADD_UNIT(ECARule30Bounds_Update)
  ADD_UNIT(ECARule30LongTime_Update)

  ADD_UNIT(ECARule30_Step)
  ADD_UNIT(ECARule30Bounds_Step)
  ADD_UNIT(ECARule30LongTime_Step)
END_SUITE
