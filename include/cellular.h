// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

typedef void (*cellular_update_t)(int*, size_t, void const*);
typedef int (*cellular_step_t)(int*, int const*, size_t, void const*);
int cellular_default_step(int *dst, int const *src, size_t n,
    void const *rule);

#define CELLULAR_RULE cellular_update_t update; \
  cellular_step_t step

void cellular_automaton_update(int *ca, size_t n, void const *rule);
int cellular_automaton_step(int *dst, int const *src, size_t n,
    void const *rule);

typedef struct cellular_eca
{
  CELLULAR_RULE;
  uint8_t code;
} cellular_eca;

cellular_eca cellular_eca_new(uint8_t code);

typedef struct cellular_oeca
{
  CELLULAR_RULE;
  uint8_t code;
  uint8_t left, right;
} cellular_oeca;

cellular_oeca cellular_oeca_new(uint8_t code, uint8_t left, uint8_t right);

#ifdef __cplusplus
}
#endif
