// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include <cellular.h>
#include <string.h>

struct cellular_rule
{
  cellular_update_t update;
  cellular_step_t step;
};

int cellular_default_step(int *dst, int const *src, size_t n, void const *rule)
{
  if (dst && src && n != 0)
  {
    memcpy(dst, src, n*sizeof(int));
    cellular_automaton_update(dst, n, rule);
    return (int)n;
  }
  return 0;
}

void cellular_automaton_update(int *ca, size_t n, void const *rule)
{
  struct cellular_rule const *r = rule;
  r->update(ca, n, rule);
}

int cellular_automaton_step(int *dst, int const *src, size_t n, void const *rule)
{
  struct cellular_rule const *r = rule;
  return r->step(dst, src, n, rule);
}

static void cellular_eca_update(int *ca, size_t n, void const *rule)
{
  cellular_eca const *r = rule;
  
  int const a = 1 & ca[0];
  int d = 7 & (((1 & ca[n-1]) << 2) | ((1 & ca[0]) << 1) | (1 & ca[1]));
  ca[0] = 1 & ((r->code & (1 << d)) >> d);
  for (size_t i = 1; i < n-1; ++i)
  {
    d = 7 & ((d << 1) | (1 & ca[i+1]));
    ca[i] = 1 & ((r->code & (1 << d)) >> d);
  }
  d = 7 & ((d << 1) | a);
  ca[n-1] = 1 & ((r->code & (1 << d)) >> d);
}

cellular_eca cellular_eca_new(uint8_t code)
{
  return (cellular_eca){
    .update = cellular_eca_update,
    .step = cellular_default_step,
    .code = code
  };
}

static void cellular_oeca_update(int *ca, size_t n, void const *rule)
{
  cellular_oeca const *r = rule;

  int d = 7 & (((1 & r->left) << 2) | (1 & ca[0]) << 1 | (1 & ca[1]));
  ca[0] = 1 & ((r->code & (1 << d)) >> d);
  for (size_t i = 1; i < n - 1; ++i)
  {
    d = 7 & ((d << 1) | ca[i+1]);
    ca[i] = 1 & ((r->code & (1 << d)) >> d);
  }
  d = 7 & ((d << 1) | r->right);
  ca[n-1] = 1 & ((r->code & (1 << d)) >> d);
}

cellular_oeca cellular_oeca_new(uint8_t code, uint8_t left, uint8_t right)
{
  return (cellular_oeca){
    .update = cellular_oeca_update,
    .step = cellular_default_step,
    .code = code,
    .left = left,
    .right = right
  };
}
